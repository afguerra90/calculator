import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FormInputComponent } from './common/form-input/form-input.component';
import { FormSelectComponent } from './common/form-select/form-select.component';
import { LoadingComponent } from './common/loading/loading.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { NumbersOnlyDirective } from './common/NumbersOnlyDirective';
import { UpperCaseTextDirective } from './common/upper-case-text-directive';

@NgModule({
  declarations: [
    NumbersOnlyDirective,
    UpperCaseTextDirective,
    LoadingComponent,
    FormSelectComponent,
    FormInputComponent,
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: false,
      tapToDismiss: true,
    }),
    AppRoutingModule
  ],
  exports: [
    ToastrModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
