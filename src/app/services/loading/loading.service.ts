import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private Visibility: BehaviorSubject<boolean>;
  activeRequests: number = 0;

  public getVisibility(): Observable<boolean> {
    return this.Visibility.asObservable();
  }

  private setVisibility(newValue: boolean): void {
    this.Visibility.next(newValue);
  }

  constructor() {
    this.Visibility = new BehaviorSubject<boolean>(true);
  }

  startLoading() {
    if (this.activeRequests === 0) {
      this.setVisibility(true);
    }
    // this.activeRequests++;
    // Utils.log("ind +"+this.activeRequests);
  }
  stopLoading() {
    // this.activeRequests--;
    // Utils.log("ind -"+this.activeRequests);
    if (this.activeRequests <= 0) {
      this.activeRequests = 0;
      this.setVisibility(false);
    }
  }
}
