import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { IResponseDto, responseDto } from 'src/app/models/responseDto';
import { Observable, throwError } from 'rxjs';
import { Utils } from 'src/app/common/utils';
import { Constants } from 'src/app/common/Constants';
import { map, retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiHelperService {
  baseurl: string;

  constructor(private http: HttpClient,
    @Inject('BASE_URL') _baseUrl: string,
    private utils: Utils
  ) {
    this.baseurl = _baseUrl;

  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    }),
    body: undefined,
    params: undefined
  }


  // Error handling
  errorHandler(errorObj) {
    let resultError: IResponseDto;
    if (errorObj instanceof HttpErrorResponse) {
      if (errorObj.status == 500) {
        resultError = new responseDto<object>().deserialize({
          estado: false,
          mensaje: errorObj.error.mensaje,
        });
      }
      else if (errorObj.status !== 400) {
        resultError = new responseDto<object>().deserialize({
          estado: false,
          mensaje: Constants.msgError
        });
      } else {
        resultError = errorObj.error
      }
    }
    else {
      resultError = errorObj;
    }
    Utils.log(JSON.stringify(errorObj));
    return throwError(resultError);
  }

  complete<TResponse>(responseJson, _utils: Utils) {
    var result = new responseDto<TResponse>().deserialize(responseJson);
    return result.dato;
  }

  // POST
  public apiPost<TRequest, TResponse>(endPoint: string, data: TRequest): Observable<TResponse> {
    return this.http.post<responseDto<TResponse>>(this.baseurl + endPoint, JSON.stringify(data), this.httpOptions)
      .pipe(
        map(responseJson => this.complete<TResponse>(responseJson, this.utils)),
        retry(1),
        catchError(error => this.errorHandler(error))
      )
  }


  // PUT
  public apiPut<TRequest, TResponse>(endPoint: string, data: TRequest): Observable<TResponse> {

    return this.http.put<responseDto<TResponse>>(this.baseurl + endPoint, JSON.stringify(data), this.httpOptions)
      .pipe(
        map(responseJson => this.complete<TResponse>(responseJson, this.utils)),
        retry(1),
        catchError(error => this.errorHandler(error))
      )
  }

  // DELETE
  public apiDelete<TRequest, TResponse>(endPoint: string, data: TRequest): Observable<TResponse> {
    this.httpOptions.params = data;
    return this.http.delete<responseDto<TResponse>>(this.baseurl + endPoint, this.httpOptions)
      .pipe(
        map(responseJson => this.complete<TResponse>(responseJson, this.utils)),
        retry(1),
        catchError(error => this.errorHandler(error))
      )
  }
  // DELETE
  public apiDeleteParams<TRequest, TResponse>(endPoint: string, data: TRequest): Observable<TResponse> {
    this.httpOptions.body = JSON.stringify(data);
    return this.http.delete<responseDto<TResponse>>(this.baseurl + endPoint, this.httpOptions)
      .pipe(
        map(responseJson => this.complete<TResponse>(responseJson, this.utils)),
        retry(1),
        catchError(error => this.errorHandler(error))
      )
  }
  //GET
  public apiGet<TResponse>(endPoint: string): Observable<TResponse> {
    return this.http.get<responseDto<TResponse>>(this.baseurl + endPoint, this.httpOptions)
      .pipe(
        map(responseJson => this.complete<TResponse>(responseJson, this.utils)),
        retry(1),
        catchError(error => this.errorHandler(error))
      )
  }

  //GET
  public apiGetParams<TResponse>(endPoint: string, query: any): Observable<TResponse> {
    this.httpOptions.params = query;
    return this.http.get<responseDto<TResponse>>(this.baseurl + endPoint, this.httpOptions)
      .pipe(
        map(responseJson => this.complete<TResponse>(responseJson, this.utils)),
        retry(1),
        catchError(error => this.errorHandler(error))
      )
  }
  // POST
  public apiPostParams<TResponse>(endPoint: string, query: any): Observable<TResponse> {
    return this.http.post<responseDto<TResponse>>(this.baseurl + endPoint, query, this.httpOptions)
      .pipe(
        map(responseJson => this.complete<TResponse>(responseJson, this.utils)),
        retry(1),
        catchError(error => this.errorHandler(error))
      )
  }
}
