import { Injectable } from '@angular/core';
import { ModalComponent } from 'src/app/modal/modal.component';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(
    private ngbModal: NgbModal,
  ) { }

  modalOptions: NgbModalOptions = {
    backdrop: 'static',
    backdropClass: 'customBackdrop',
    centered: true, 
  };
  public modalRef: NgbModalRef;
  public showSi_No(titulo: string, mensaje: string) {
    this.modalRef = this.ngbModal.open(ModalComponent, this.modalOptions);
    this.modalRef.componentInstance.titulo = titulo;
    this.modalRef.componentInstance.mensaje = mensaje;
  }

}
