import { Directive, HostListener, Input } from '@angular/core';
import { Utils } from './utils';

@Directive({
  selector: '[onlyNumbers]'
})
export class NumbersOnlyDirective {

  regexStr = '^[0-9]*$';
  constructor() { }

  @Input() onlyNumbers: boolean;

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    let e = event;
    if (this.onlyNumbers == true) {
      if ([46, 8, 9, 27, 13].indexOf(e.keyCode) !== -1) {
        return;
      }
      if ([190, 110].indexOf(e.keyCode) !== -1) {
        e.preventDefault();
        return;
      }

      if (
        //Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        //Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
        //Allow: Ctrl+V
        (e.keyCode == 86 && e.ctrlKey === true) ||
        //Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
        //Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        //let it happen, don't do anything 
        e.preventDefault();
        return;
      }
      let ch = e.key;
      let regEx = new RegExp(this.regexStr);
      if (regEx.test(ch))
        return;
      else
        e.preventDefault();
    }
  }
}