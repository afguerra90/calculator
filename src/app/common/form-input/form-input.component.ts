import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { Constants } from '../Constants'; 

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.css']
})
export class FormInputComponent implements OnInit {

  @Input()
  accept: any;

  @Input()
  label: string;

  @Input()
  controlName: any;

  @Input()
  type: any;

  @Input()
  submitted: boolean;

  @Input()
  id: string;

  @Input()
  minValue: any = undefined;

  @Input()
  maxlength: number = 256;

  @Input()
  soloNumeros: boolean = false;

  @Input()
  mayusculas: boolean = false;

  @Input()
  classInput: string;

  fileName: string = "";

  get fileNameseleccionado(): string {
    return this.fileName == "" ? "Seleccione archivo" : this.fileName;
  }

  @Output() upLoad = new EventEmitter<string>();
 
  constructor(private parentF: FormGroupDirective) { }

  ngOnInit() {

  }
  get msgRequired() { return Constants.msgRequired; }
  get msgMaxLength() { return Constants.msgMaxLength; }
  get msgMinLength() { return Constants.msgMinLength; }
  get parentform() { return this.parentF.form; }
  get control() { return this.parentF.form.get(this.controlName); }
  get valid() {
    return ((this.submitted && this.control.errors) || (this.control.errors && (this.control.dirty || this.control.touched)));
  }


}
