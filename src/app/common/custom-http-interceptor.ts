import { finalize } from "rxjs/operators";
import { Injectable } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { HttpRequest } from '@angular/common/http';
import { HttpHandler } from '@angular/common/http';
import { HttpEvent } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoadingService } from "../services/loading/loading.service";
import { Utils } from "./utils";

@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {

    activeRequests: number = 0;

    constructor(private loadingService: LoadingService,
    ) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.activeRequests === 0) {
            this.loadingService.startLoading();
        }
        this.activeRequests++;
        return next.handle(request).pipe(
            finalize(() => {
                this.activeRequests--;
                if (this.activeRequests === 0) {
                    this.loadingService.stopLoading();
                }
            })
        )
    };

}