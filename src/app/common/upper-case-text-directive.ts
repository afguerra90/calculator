import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
    selector: '[upperCase]'
})
export class UpperCaseTextDirective {

    constructor(public ref: ElementRef) { }

    @Input() upperCase: boolean;

    @HostListener('input', ['$event']) onInput(event) {
        if (this.upperCase == true) {
            this.ref.nativeElement.value = event.target.value.toUpperCase();

        }
    }

}