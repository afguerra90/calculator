import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {
  public isVisible: boolean = true; 

  constructor(private LoadingService: LoadingService) {
    this.isVisible = true; 
    this.LoadingService.getVisibility().subscribe(value => {
      this.visibilityChange(value);
    });
  } 
  ngOnInit(): void {
    this.visibilityChange(!this.isVisible);
  }
  visibilityChange(value: boolean): void {
    this.isVisible = value;
  }
}

