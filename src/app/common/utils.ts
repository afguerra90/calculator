import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  

@Injectable({
    providedIn: 'root'
})
export class Utils { 
    constructor(
        private toastr: ToastrService,
    ) { 
    }

    public static log(error: any) {
        console.log(error);
    }

    public onError(error: any) {
        Utils.log(error);
        this.showError("Verbo Divino", error);
    }

    showSuccess(title, message) {
        this.toastr.success(message, title)
    }

    showError(title, message) {
        this.toastr.error(message, title)
    }

    showInfo(title, message) {
        this.toastr.info(message, title)
    }

    showWarning(title, message) {
        this.toastr.warning(message, title)
    }
}