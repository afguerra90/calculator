import { Component, OnInit, Input } from '@angular/core';
import { Constants } from '../Constants';
import { FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.css']
})
export class FormSelectComponent implements OnInit {

  @Input()
  controlName: any;

  @Input()
  valueChange: any;

  @Input()
  datos: any[];

  @Input()
  label: string;

  @Input()
  id: string;

  @Input()
  nombreDescripcion: string;

  @Input()
  nombreId: string;

  @Input()
  submitted: boolean;

  constructor(private parentF: FormGroupDirective) { }

  ngOnInit() {
  }
  valueDefault() { return null; }
  get msgRequired() { return Constants.msgRequired; }
  get parentform() { return this.parentF.form; }
  get control() { return this.parentF.form.get(this.controlName); }
  get valid() {
    return ((this.submitted && this.control.errors) || (this.control.errors && (this.control.dirty || this.control.touched)));
  }

  getDescripcion(object: any) {
    if (object != undefined)
      return object[this.nombreDescripcion as keyof typeof object];
  }
  getId(object: any) {
    if (object != undefined)
      return object[this.nombreId as keyof typeof object];
  }
  change(event) {
    if (this.valueChange)
      this.valueChange(event);
  }
}
