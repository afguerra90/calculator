import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public submitted: boolean = false;
  public form: FormGroup;

  constructor(
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.submitted = false;
    this.form = this.formBuilder.group({
      IdTecnico: [null, [Validators.required, Validators.minLength(10)]],
      IdServicio: [null, [Validators.required, Validators.minLength(10)]],
      FechaI: [null, [Validators.required]],
      FechaF: [null, [Validators.required]],
    }, { validator: this.fechaValidator });
  }
  onSubmit() {
    this.submitted = true;
    debugger;
    if (this.form.invalid) return;
  }

  fechaValidator(fg: FormGroup): { [s: string]: boolean } {

    if (fg) {
      const fechaI = fg.get("FechaI").value ? new Date(fg.get("FechaI").value) : null;
      const fechaF = fg.get("FechaF").value ? new Date(fg.get("FechaF").value) : null;
      // console.log(fechaF);
      // console.log(fechaI);
      // console.log(fechaI > fechaF);
      if (fechaI != null && fechaF != null && fechaI >= fechaF) {
        return { invalidFecha: true };
      }
    }
    return null;
  }

}
